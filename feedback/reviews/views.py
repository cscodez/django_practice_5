from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.views.generic.base import TemplateView  # for outputting a template as a list of data
from django.views.generic import ListView  # a more specified TemplateView, fetching a list of data based on a model
from django.views.generic import DetailView
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView

from .forms import ReviewForm
from .models import Review


# Create your views here.


class ReviewView(CreateView):
    # with CreateView you don't need to create ReviewForm Modelform in forms, just put fields = "__all__", and Django
    # creates it for you. But if you need your form to be more specific, you define form_class variable, and you
    # then connect to your defined form class.
    # There are also UpdateView and DeleteView classes as well
    model = Review
    form_class = ReviewForm
    template_name = "reviews/review.html"
    success_url = "/thank-you"


class ThankYouView(TemplateView):  # django will automatically return it for get requests to reach this view
    template_name = "reviews/thank_you.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["message"] = "This works!"
        return context


class ReviewsListView(ListView):
    template_name = "reviews/review_list.html"
    model = Review  # just pointing to Review class, not instantiating
    context_object_name = "reviews"  # if you set, it will be instead of the object_list

    # def get_queryset(self):
    #     base_query = super().get_queryset()
    #     data = base_query.filter(rating__gt=4)
    #     return data  # with this you can adjust queryset, now for example filtering entries with rating grater than 4


class SingleReviewView(DetailView):
    template_name = "reviews/single_review.html"
    model = Review

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        loaded_review = self.object
        request = self.request
        favourite_id = request.session.get("favourite_review")  # if not sure if it is set use get instead of [ ]
        context["is_favourite"] = favourite_id == str(loaded_review.id)  # str because loaded_review.id is int!!
        return context


class AddFavouriteView(View):
    def post(self, request):
        review_id = request.POST["review_id"]
        request.session["favourite_review"] = review_id  # always store simple types in a session, like str or int
        return HttpResponseRedirect("/reviews/" + review_id)





